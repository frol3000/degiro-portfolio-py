import os
import csv
from posixpath import join
import yaml
import sys
import datetime
import sqlite3


def initialize():

    global config

    config_keys = [
        'portfolio_path_archive',
        'portfolio_path_in',
        'data_path',
        'asset_types'
    ]

    config = yaml.safe_load(open('./config.yaml'))
    if not all(key in config for key in config_keys):
        sys.exit('Application configuration is required.')

    if not config['asset_types']:
        sys.exit('Asset types must be configured.')

    if not os.path.exists(os.path.expanduser(config['portfolio_path_in'])):
        sys.exit('Portfolio path-in does not exists.')

    if not os.path.exists(os.path.expanduser(config['portfolio_path_archive'])):
        os.mkdir(config['portfolio_path_archive'])

    if not os.path.exists(os.path.expanduser(config['data_path'])):
        os.mkdir(config['data_path'])

    initialize_db()


def get_portfolio_in():
    return os.path.expanduser(config['portfolio_path_in'])


def initialize_db():

    global db
    db = sqlite3.connect(join(config['data_path'], 'asset_allocation.db'))

    global dbc
    dbc = db.cursor()
    dbc.execute("""create table if not exists `portfolio`
                (`id` integer primary key autoincrement, created_at text not null);""")
    dbc.execute("""create table if not exists `portfolio_details`
                (
                    portfolio_id integer not null,
                    isin text not null,
                    amount integer not null,
                    value real not null
                );""")
    dbc.execute("""create table if not exists `asset_allocation`
                (
                    portfolio_id integer not null,
                    asset_type text not null,
                    amount integer not null
                );""")
    dbc.execute("""create table if not exists `isin`
                 (
                    isin text primary key not null,
                    product_name text not null,
                    asset_type text not null
                );""")
    db.commit()


def store_isin(isin, product, asset):

    dbc.execute("""INSERT INTO isin VALUES ('{}', '{}', '{}');"""
                .format(isin, product, asset))
    db.commit()


def request_asset_type(isin, product):

    asset_types = config['asset_types']
    at = input("Enter the Asset Type for '{}': ".format(product))
    if not at in asset_types:
        print("Provided Asset Type is not supported. The supported types are: {}.\nPlease try again.".format(
            ', '.join(asset_types)))
        at = request_asset_type(isin, product)
    return at


def load_isins():

    isins = dbc.execute('SELECT * FROM isin;').fetchall()
    return dict(map(lambda row: (row[0], row[2]), isins))


def load_portfolio_csv() -> dict:

    pf = open(get_portfolio_in())
    portfolio_reader = csv.reader(pf)
    header = next(portfolio_reader)

    isin_i = False
    for isins, h in enumerate(header):
        if 'isin' in h.lower():
            isin_i = isins
            break

    product_i = False
    for isins, h in enumerate(header):
        if 'produkt' in h.lower():
            product_i = isins
            break

    return {'portfolio': list(portfolio_reader), 'isin_i': isin_i, 'product_i': product_i}


def calculate_allocation(portfolio_isins, p: dict):

    asset_allocation = dict((asset, {'amount': 0, 'value': 0.00})
                            for asset in config['asset_types'])

    total = 0.00
    for row in p['portfolio']:

        isin = row[p['isin_i']]
        asset = ''
        if len(isin) > 0:
            if not isin in portfolio_isins:
                asset = request_asset_type(isin, row[product_i])
                portfolio_isins[isin] = asset
                store_isin(isin, row[p['product_i']], asset)
            else:
                asset = portfolio_isins[isin]

        if asset != '':
            amount = int(row[2].replace(',', '.'))
            value = float(row[5].replace(',', '.'))
            asset_allocation.update({asset: {
                'amount': asset_allocation[asset]['amount'] + amount,
                'value': asset_allocation[asset]['value'] + value
            }})
            total += value

    asset_allocation = dict(
        sorted(
            asset_allocation.items(),
            key=lambda item: item[1]['value'], reverse=True
        )
    )
    asset_allocation['total'] = total

    return asset_allocation


def print_results(asset_allocation):

    print('Asset allocation at {}'.format(get_portfolio_time()))
    for k, v in asset_allocation.items():
        if k == 'total':
            continue
        print('{0:>4}:{1:>12.2f}{2:>8.2f}%'.format(
            k, v['value'], v['value']/asset_allocation['total']*100))

    print('Total: {0:>10.2f}'.format(asset_allocation['total']))


def get_portfolio_time():
    return datetime.datetime.fromtimestamp(os.stat(get_portfolio_in()).st_mtime)


def save_asset_portfolio(portfolio_csv: dict):

    ptime = get_portfolio_time()
    print(ptime)

    sql = "SELECT * FROM portfolio where created_at = '{ptime}';".format(
        ptime=ptime)
    isins = dbc.execute(sql).fetchall()
    if len(isins) > 0:
        return

    dbc.execute(
        "insert into portfolio(created_at) values('{ptime}')".format(ptime=ptime))
    pid = dbc.lastrowid

    zzz = portfolio_csv.items()
    reader = portfolio_csv['portfolio']

    for row in reader:
        print(row)

        dbc.execute(
            "insert into portfolio_details(portfolio_id, isin, amount, value) values({portfolio_id}, '{isin}', {amount}, {value})"
            .format(
                portfolio_id=pid,
                isin=row[portfolio_csv['isin_i']],
                amount=int(row[2].replace(',', '.') if row[2] != '' else 0),
                value=float(row[5].replace(',', '.'))
            )
        )

    db.commit()


def shutdown():
    dbc.close()


initialize()
portfolio_isins = load_isins()
portfolio_csv = load_portfolio_csv()
asset_allocation = calculate_allocation(portfolio_isins, portfolio_csv)
print_results(asset_allocation)
save_asset_portfolio(portfolio_csv)
shutdown()
